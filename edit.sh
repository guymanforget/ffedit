#!/bin/bash
if [ -z "$@" ]; then
echo 'No filename given. Proper syntax is "ffedit [filename]".';
exit 1
fi

file=$@

while true; do
if [ $(tput cols) -lt 98 ]; then
echo -n "╼"
i=1; while [ $i -lt $(($(tput cols)-2)) ]; do
echo -n "═"
i=$(($i + 1))
done
echo -n "╾"
else
echo ╼════════════════════════════════════════════════════════════════════════════════════════════════╾
fi
option=$(echo "change_file\
	delete_section\
	join\
	text\
	black_space\
	tint\
	chroma_key\
	view_contents\
	ffmpeg_command\
	quit" | smenu -m "File: $file")
case $option in
	"change_file")
	filetmp=$(echo "$( ls *.mp4 )\ cancel" | smenu -w)
	if [ $filetmp != "cancel" ]; then
		file=$filetmp
	fi
	;;
	"delete_section")
	echo Incomplete
	;;
	"join")
	echo Incomplete
	;;
	"text")
	echo Incomplete
	;;
	"black_space")
	echo Incomplete
	;;
	"tint")
	echo Incomplete
	;;
	"chroma_key")
	echo Incomplete
	;;
	"view_contents")
	echo Incomplete
	;;
	"ffmpeg_command")
	echo Incomplete
	;;
	"quit")
	break
	;;
esac
done
